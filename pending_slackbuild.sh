#!/bin/bash

if [[ $# -lt 1 ]]; then
	echo "Usage: $0 pattern_to_match"
	echo "Du måste ange en söksträng" >&2
	exit 2
fi

PATTERN="$@"
CONTENT="$(curl -s https://slackbuilds.org/pending/)"
LINES=$(echo "$CONTENT" | grep "<tr>" | wc -l)
MATCH=$(echo "$CONTENT" | grep $PATTERN)

if [ -z "${MATCH}" ]
then
	echo "$PATTERN är behandlad!"
else
	echo "$PATTERN är inte behandlad. Totalt $LINES paket pending"
fi


